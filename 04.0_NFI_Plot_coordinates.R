##-------------------------------------------------------------------------------------------------------------------
## Calculation and analysis of production functions between forest structural parameters and 
## ecosystem service proxies in Germany
#
## The analysis was conducted as part of the project "BioHolz-Projekt" (www.bioholz-projekt.de)
##    funded by the in the funding program 'Research for the Implementation of the National Biodiversity 
##    Strategy (F&U NBS)' by the German Federal Ministry for Education and Research (BMBF) and the 
##    German Federal Agency for Nature Conservation (BfN) with funds provided by the German Federal 
##    Ministry for the Environment, Nature Conservation, Building and Nuclear Safety (BMUB).
## The majority of data used were sampled within the Biodiversity Exploratories (www.biodiversity-exploratories.de)
#
## Analysis and results are published as:
## Simons Nadja K., María R. Felipe-Lucia, Peter Schall, Christian Ammer, Jürgen Bauhus, Nico Blüthgen,
## Steffen Boch, François Buscot, Markus Fischer, Kezia Goldmann, Martin M. Gossner, Falk Hänsel, 
## Kirsten Jung, Peter Manning, Thomas Nauss, Yvonne Oelmann, Rodica Pena, Andrea Polle, Swen C. Renner,
## Michael Schloter, Ingo Schöning, Ernst-Detlef Schulze, Emily F. Solly, Elisabeth Sorkau, 
## Barbara Stempfhuber, Tesfaye Wubet, Jörg Müller, Sebastian Seibold & Wolfgang W. Weisser (ADD YEAR):
## Forest inventories capture the multifunctionality of managed forests. ADD JOURNAL & DOI
# 
## R-scripts were mainly written by Nadja K. Simons (nadja.simons@tu-darmstadt.de)
## Last updated: 11. April 202
#
##------------------------------------------------------------------------------------------------------------------

# Combine the anonymized plot coordinates with information on predictors, estimated ecosystem services
# and forest type.
# If the correct geographic coordinates are available to the user, a spatialpointsdataframe should be created
# using the file "01.2_Geographic_Environment_Information": Inventory_latlon.spdf

# Check plot coordinate data
length(unique(NFI_shp@data$Tnr))
length(unique(NFI_shp@data$cell_id))
# each grid cell (1km by 1km) is associated with one Tracktnumber
# each cell/Trackt is a polygon with several coordinates

NFI_shp.sppoint <- as.data.frame(raster::geom(NFI_shp))
names(NFI_shp.sppoint)
length(unique(NFI_shp.sppoint$object))
NFI_shp.sppoint %>% group_by(object) %>% summarise(n_points = n()) %>% summary() # each object has 5 coordinates
plot(NFI_shp.sppoint[1:5,5]~NFI_shp.sppoint[1:5,6]) # one corner-point is double

# For plotting purposes, we pretend that each grid corner corresponds to one NFI plot per Trackt
# 1. get object IDs to match
head(NFI_shp@data)
head(NFI_shp.sppoint)

dim(NFI_shp@data)
NFI_shp.sppoint %>% group_by(object) %>% summarise(n_points = n()) %>% nrow()

IDs <- rep(NA,length(NFI_shp@polygons))
for(i in 1:length(NFI_shp@polygons)){
  IDs[i] <- as.numeric(NFI_shp@polygons[[i]]@ID)
}

# 2. Add the IDs to the coordinate table
sort(rep(IDs,5)) -> IDs
dim(NFI_shp.sppoint)
length(IDs)

cbind(NFI_shp.sppoint,"ID"=IDs) -> NFI_shp.sppoint

# 3. Extract information for each Trackt from spatial object
NFI_shp_data <- NFI_shp@data
head(NFI_shp_data)
NFI_shp_data$ID <- as.numeric(row.names(NFI_shp_data))

# 4. Combine coordinates and Trackt information
left_join(NFI_shp.sppoint,NFI_shp_data, by = "ID") -> NFI_point_data

# 5. Create dummy for PlotID
NFI_point_data$Enr <- rep(seq(1:5),length(NFI_shp@polygons))
NFI_point_data %>% mutate(PlotID = paste(Tnr,Enr,sep = "_")) -> NFI_point_data

## select only those plots which were used to calculate stand structures
NFI_plots <- NFI_point_data[NFI_point_data$Tnr %in% NFI_Stand_structure$Tnr,]
dim(NFI_plots)
head(NFI_plots)

## add further variables to the dataframe 
#----------------------------------------------

# Select only predictors used for modlling
NFI_Strata1BT_data_subset %>%  dplyr::select(one_of("PlotID",predictors_plotting_order)) -> NFI_Strata_predictors

# Get the information for forest type definition
NFI_StrataSelection_1BT %>% dplyr::select(PlotID,NatWgF,natNaehe,Wg,BestockTypFein,Strata_1) -> NFI_Strata_info

# Change the order of columns in the ecosystem services data.frame to match plotting order
NFI_Strata_ES_avg %>%  dplyr::select(one_of("PlotID","Strata",ES_plotting_order)) -> NFI_Strata_ES_avg
NFI_Strata_ES_lowerCI %>%  dplyr::select(one_of("PlotID","Strata",ES_plotting_order)) -> NFI_Strata_ES_lowerCI
NFI_Strata_ES_upperCI %>%  dplyr::select(one_of("PlotID","Strata",ES_plotting_order)) -> NFI_Strata_ES_upperCI

# Add all information to spatial coordinate table
NFI_plots %>% dplyr::select(PlotID,Tnr,Enr,cell_id,EPSG,x,y) %>% 
  left_join(NFI_Strata_predictors, by = "PlotID") %>%
  left_join(NFI_Strata_info, by = "PlotID") %>%
  left_join(NFI_Strata_ES_avg, by = "PlotID") -> NFI_plots_allInfo_ESavg
NFI_plots %>% dplyr::select(PlotID,Tnr,Enr,cell_id,EPSG,x,y) %>% 
  left_join(NFI_Strata_predictors, by = "PlotID") %>%
  left_join(NFI_Strata_info, by = "PlotID") %>%
  left_join(NFI_Strata_ES_lowerCI, by = "PlotID") -> NFI_plots_allInfo_ESlowerCI
NFI_plots %>% dplyr::select(PlotID,Tnr,Enr,cell_id,EPSG,x,y) %>% 
  left_join(NFI_Strata_predictors, by = "PlotID") %>%
  left_join(NFI_Strata_info, by = "PlotID") %>%
  left_join(NFI_Strata_ES_upperCI, by = "PlotID") -> NFI_plots_allInfo_ESupperCI

dim(NFI_plots_allInfo_ESavg)
summary(NFI_plots_allInfo_ESavg)

NFI_plots_allInfo_ESavg %>% filter(Enr == 5) %>% summary() # all NA

NFI_plots_allInfo_ESavg %>% filter(Enr != 5) -> NFI_plots_allInfo_ESavg
NFI_plots_allInfo_ESlowerCI %>% filter(Enr != 5) -> NFI_plots_allInfo_ESlowerCI
NFI_plots_allInfo_ESupperCI %>% filter(Enr != 5) -> NFI_plots_allInfo_ESupperCI

summary(NFI_plots_allInfo_ESavg)

## Add colors for plotting
#---------------------------------
# Define colors for the different ecosystem services
# plotclr <- brewer.pal(9, "YlGnBu")  # "YlGnBu" "YlOrRd" "PuRd" "BuPu"
# Add a colour for NAs
NAColor <- "gray80"

colfun <- colorRampPalette(c("#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"))
plotclr <- colfun(51)
lgdcolor <- c(rev(plotclr), NAColor)

colcodes_ES <- list()

for(i in ES_high_certainty+25) {
  cuts <- classIntervals(NFI_plots_ESavg.spdf@data[[i]], n=51, style="equal")
  # Using the findColours function we can assign colors to our points without breaking the associated order
  colcode <- findColours(cuts, colfun(51))
  colcodes_ES[[length(colcodes_ES)+1]] <-  colcode
}

names(colcodes_ES) <- names(NFI_plots_ESavg.spdf@data)[ES_high_certainty+25]

color.bar <- function(lut, min, max=-min, nticks=11, ticks=seq(min, max, len=nticks), title='') {
  scale = (length(lut)-1)/(max-min)
  plot(c(min,max),c(0,10), type='n', bty='n', xaxt='n', xlab='', yaxt='n', ylab='', main=title)
  axis(1, ticks, las=1)
  for (i in 1:(length(lut)-1)) {
    x = (i-1)/scale + min
    rect(x,0,x+1/scale,10, col=lut[i], border=NA)
  }
}

Wg_clr <- data.frame("color_Wg" = c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", "#77AADD", "#117777", 
                                    "#44AAAA", "#77CCCC", "#117744", "#44AA77", "#88CCAA", "#777711", "#AAAA44", 
                                    "#DDDD77", "#774411", "#AA7744", "#DDAA77", "#771122","gray80"),
                     "Wg" = c(2,13,17,22,35,36,37,56,60,61,64,66,70,72,73,75,76,77,79,NA))

BestockTyp_clr <- data.frame("color_BT" = c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", 
                                            "#117777", "#44AAAA", "#77CCCC","#44AA77", "#DDAA77", "#771122","gray80"),
                             "BestockTypFein" = c(2100,2101,2103,2200,2201,2600,2601,2602,2603,2701,3101,NA))

BestockTyp_Sc_clr <- data.frame("color_BT" = c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", 
                                               "#117777", "#44AAAA", "#77CCCC","#44AA77", "#DDAA77", "#771122","gray80"),
                                "BestockTypNew" = c(2100,2101,2103,2200,2201,2600,2601,2602,2603,2701,3101,NA))


NAColor <- "gray80"
lgdcolor_Wg <- c(rev(Wg_clr$color), NAColor)
lgdcolor_BestockTyp <- c(rev(BestockTyp_clr$color), NAColor)

# Add colors to data
NFI_plots_allInfo_ESavg %>% left_join(Wg_clr) %>% left_join(BestockTyp_clr) -> NFI_plots_allInfo_ESavg
NFI_plots_allInfo_ESavg$color_Wg[is.na(NFI_plots_allInfo_ESavg$Root_decomp)] <- "gray80"
NFI_plots_allInfo_ESavg$color_BT[is.na(NFI_plots_allInfo_ESavg$Root_decomp)] <- "gray80"

NFI_plots_allInfo_ESlowerCI %>% left_join(Wg_clr) %>% left_join(BestockTyp_clr) -> NFI_plots_allInfo_ESlowerCI
NFI_plots_allInfo_ESlowerCI$color_Wg[is.na(NFI_plots_allInfo_ESlowerCI$Root_decomp)] <- "gray80"
NFI_plots_allInfo_ESlowerCI$color_BT[is.na(NFI_plots_allInfo_ESlowerCI$Root_decomp)] <- "gray80"

NFI_plots_allInfo_ESupperCI %>% left_join(Wg_clr) %>% left_join(BestockTyp_clr) -> NFI_plots_allInfo_ESupperCI
NFI_plots_allInfo_ESupperCI$color_Wg[is.na(NFI_plots_allInfo_ESupperCI$Root_decomp)] <- "gray80"
NFI_plots_allInfo_ESupperCI$color_BT[is.na(NFI_plots_allInfo_ESupperCI$Root_decomp)] <- "gray80"


##### FINAL STEP: CREATE SPATIAL OBJECTS #####
NFI_plots_ESavg.spdf <- SpatialPointsDataFrame(NFI_plots_allInfo_ESavg[,c("x","y")],
                                               data = NFI_plots_allInfo_ESavg[c(1:5,8:44)])
projection(NFI_plots_ESavg.spdf) <- projection(NFI_shp) # Define projection

NFI_plots_ESlowerCI.spdf <- SpatialPointsDataFrame(NFI_plots_allInfo_ESlowerCI[,c("x","y")],
                                                   data = NFI_plots_allInfo_ESlowerCI[c(1:5,8:44)])
projection(NFI_plots_ESlowerCI.spdf) <- projection(NFI_shp) # Define projection

NFI_plots_ESupperCI.spdf <- SpatialPointsDataFrame(NFI_plots_allInfo_ESupperCI[,c("x","y")],
                                                   data = NFI_plots_allInfo_ESupperCI[c(1:5,8:44)])
projection(NFI_plots_ESupperCI.spdf) <- projection(NFI_shp) # Define projection

# Make copy of spatial object with German outline with correct projection

germany_tmerc <- spTransform(germany,crs(NFI_plots_ESavg.spdf))

##############################################
## CODE WITH Inventory_latlon             ####
##############################################
# Add predicted ecosystem services to the spatial coordinates
# Inventory_latlon %>% dplyr::select(c(PlotID,bl,LAT_MEAN,LON_MEAN)) %>% left_join(NFI_Strata_ES_avg,by="PlotID") -> ES_latlon
# Inventory_latlon %>% dplyr::select(c(PlotID,bl,LAT_MEAN,LON_MEAN)) %>% left_join(NFI_Strata_ES_lowerCI,by="PlotID") -> lowerCI_latlon
# Inventory_latlon %>% dplyr::select(c(PlotID,bl,LAT_MEAN,LON_MEAN)) %>% left_join(NFI_Strata_upperCI,by="PlotID") -> upperCI_latlon
# 
# ES_latlon.spdf <- SpatialPointsDataFrame(ES_latlon[,c("LON_MEAN","LAT_MEAN")],data = ES_latlon[c(1,2,5:20)])
# projection(ES_latlon.spdf) <- projection(germany) # Define projection as WGS84
# 
# lowerCI_latlon.spdf <- SpatialPointsDataFrame(lowerCI_latlon[,c("LON_MEAN","LAT_MEAN")],data = lowerCI_latlon[c(1,2,5:20)])
# projection(lowerCI_latlon.spdf) <- projection(germany) # Define projection as WGS84
# 
# upperCI_latlon.spdf <- SpatialPointsDataFrame(upperCI_latlon[,c("LON_MEAN","LAT_MEAN")],data = upperCI_latlon[c(1,2,5:20)])
# projection(upperCI_latlon.spdf) <- projection(germany) # Define projection as WGS84
# 
