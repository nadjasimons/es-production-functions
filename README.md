# README

This repository contains R-files to reproduce the calculations and analysis of production functions between forest structural parameters and ecosystem services in Germany.

Detailed instructions on how to use the scripts and access the data are also provided in the file 00_README.Rmd.
This file can be knitted into a .htlm file in RStudio for easy readability and as a reference.

## Preparation

Please set up the following folder structure: 

* MAIN_FOLDER/00_Data
* MAIN_FOLDER/01_Scripts
* MAIN_FOLDER/02_Outputs

The MAIN_FOLDER is the directory of your choice, which should contain the three subfolders. All R-files of this repository need to be saved in the folder "01_Scripts". All other files should be saved in the folder "00_Data". All figures or tables produced by the scripts will automatically be saved into the subfolder "02_Outputs."

## Download public data

The following data from the Biodiversity Exploratories are publicly available and can be downloaded from <https://www.bexis.uni-jena.de/PublicData/About.aspx>.
The download has to be done manually in order to ensure agreement with the data license. Under the tab "Search data" you can enter the datasetID in order to find the dataset (and related datasets). The downloaded .txt files should be saved in the folder "00_Data".

|DatasetID |  Description                                                 |
|----------|--------------------------------------------------------------|
| 20366    |  Long list of cover per plant species and vegetation layer   |
| 11603    |  Information on plot conditions, e.g. elevation, slope       |
| 22868    |  Mean annual increment as a proxy for timber production      |
| 19286    |  P content in the soil                                       |
| 17086    |  C content in the soil                                       |
| 19847    |  N content in the soil                                       |
| 19866    |  Dung decomposition                                          |
| 23289    |  Soil and root-associated fungi                              |
| 23287    |  Functional grouping of soil and root-associated fungi       |
| 21047    |  Fungi data for edible fungi                                 |
| 20035    |  Bark beetle control                                         |
| 17706    |  Description of forest type                                  |
| 17687    |  Forest stand structure and composition                      |

The following additional data are publicly available and need to be downloaded manually. The resulting data folders need to be saved in the folder "00_Data".

| Name                    | Description                                           | Instructions                           | Source                        |
|-------------------------|-------------------------------------------------------|----------------------------------------|-------------------------------|
| DEU_adm_shp             | outline of Germany and administrative areas           | select Germany and download shape-file |<https://gadm.org/download_country_v3.html> | 
| Tnr_INSPIRE_Poly1000.shp| Anonymized locations of German Forest Inventory plots | download and extract zip-file          |<https://bwi.info/Download/de/BWI-Basisdaten/ACCESS2003/>| 

**Climate data**

Climate data from the weather stations on the plots of the Biodiversity Exploratories are publicly available through <https://www.bexis.uni-jena.de/PublicData/ClimateDataPublic/ClimateDataPublicAbout.aspx>. To reproduce the data used in the published analyses, the following selections need to be made:

* Click the tab "Export data"
* Click the button "plots"
* In the drop-down on the right, select the three options including "Wald" (forest) and move the respective plots to the selection using the < button
* Once 150 plots are selected, click "apply chosen plots"
* Click button "parameters"
* From the list on the right, select "Ta_200_max" & "Ta_200_min" and move to the selection using the < button
* Click "apply chosen parameters"
* Click "timespan and aggregation"
* For "Aggregation of time" select "day"
* For "time span" select "one year" and choose 2011
* Click "apply time"
* Click "settings"
* For "quality check of measured values" select option 3
* Other settings are optional
* Click "apply settings"
* Click "create zip file" and then the link "Click to download"
* Save the downloaded files in the folder "00_Data" and extract the files into a subfolder "climate_data"

Further preparation of the data are provided in the RScript "01.3_Check_prepare_data.R"

**Forest structures from the German Forest Inventory**

The raw data of the German National Forest Inventory (BWI) are publicly available through <https://bwi.info/start.aspx>. To reproduce the data used in the published analyses, the following steps need to be made:

* Click the option "download NFI data"
* On the following page, select "BWI-Basisdaten" (the other files are optional)
* Click "ACCESS2003" (other files provide additional information)
* Click "bwi20150320_alle_daten2012.zip" to download the raw data
* Extract the database file and open in Microsoft Access
* Use the following SQL code blocks to create aggregated tables
* Export the aggregated tables to .txt files and save in the folder "00_Data"

Further preparation steps are provided in the RScript "01.3_Check_prepare_data.R"

*SQL query for tbl_DE_Bestandesstrukturen.txt:*

```
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_tab.HoeheNN, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_bestock.GT4m, b3_bestock.SumG_haH, b3_bestock.LE4m INTO tbl_DE_Bestandesstrukturen
FROM ((b0_ecke LEFT JOIN b3_bestock ON (b0_ecke.Tnr = b3_bestock.Tnr) AND (b0_ecke.Enr = b3_bestock.Enr)) LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) LEFT JOIN b0_tab ON 
b0_ecke.Tnr = b0_tab.Tnr
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with basic information of the plot, e.g. number of trees, total basal area of trees, elevation.

*SQL query for tbl_DE_AnteilBaumarten.txt:*

```
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_bestock_baanteile.Ba, b3_bestock_baanteile.Schicht, b3_bestock_baanteile.AnteilBa INTO tbl_DE_AnteilBaumarten
FROM (b0_ecke LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) INNER JOIN b3_bestock_baanteile ON (b0_ecke.Enr = b3_bestock_baanteile.Enr) AND (b0_ecke.Tnr = b3_bestock_baanteile.Tnr)
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with the proportional cover of every tree species per plot.

*SQL query for tbl_DE_Einzelbaeume:*

```
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_baeume.Bnr, b3_baeume.Av, 
b3_baeume.Ba, b3_baeume.Bhd, b3_baeume.G, b3_baeume.N_ha, b3_baeume.Stf, b3_baeume.VolR, b3_baeume.Hoehe, 
b3_baeume.Al_ba, b3_baeume.Biom_o, b3_baeume_var.K_BW INTO tbl_DE_Einzelbaeume
FROM ((b0_ecke LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) LEFT JOIN 
b3_baeume ON (b3_ecke_w.Enr = b3_baeume.Enr) AND (b3_ecke_w.Tnr = b3_baeume.Tnr)) LEFT JOIN b3_baeume_var ON (b3_baeume.Bnr = b3_baeume_var.Bnr) AND (b3_baeume.Enr = b3_baeume_var.Enr) AND (b3_baeume.Tnr = b3_baeume_var.Tnr)
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with basic information on individual trees, e.g. diamater at breast height, basal area.

*SQL query for tbl_DE_Bodenvegetation:*

```
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_boden.BodStrBa, b3_boden.Dichte INTO tbl_DE_Bodenvegetation
FROM (b0_ecke LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) 
LEFT JOIN b3_boden ON (b3_ecke_w.Enr = b3_boden.Enr) AND (b3_ecke_w.Tnr = b3_boden.Tnr)
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with the cover of selected species in the shrub layer.

*SQL query for tbl_DE_Regeneration:*

```
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_bestock_le4m.BeDG, b3_bestock_le4m_ba.Ba, b3_bestock_le4m_ba.Anteil INTO tbl_DE_Regeneration
FROM ((b0_ecke LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) LEFT JOIN b3_bestock_le4m ON (b0_ecke.Tnr = b3_bestock_le4m.Tnr) AND (b0_ecke.Enr = b3_bestock_le4m.Enr)) LEFT JOIN b3_bestock_le4m_ba ON (b0_ecke.Tnr = b3_bestock_le4m_ba.Tnr) AND (b0_ecke.Enr = b3_bestock_le4m_ba.Enr)
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with the cover of young trees in the understorey which are attributed to natural regeneration.

*SQL query for tbl_DE_Totholz:*

``` 
SELECT b0_ecke.Tnr, b0_ecke.Enr, b0_ecke.Bl, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_tot.Nr, b3_tot.Tbagr, b3_tot.Tart, b3_tot.Tzg, b3_tot.Tmd, b3_tot.Tsd, b3_tot.Tbd, b3_tot.Tl, b3_tot.Anz, b3_tot.Tm3W10, b3_tot.TBiomW10 
INTO tbl_DE_Totholz
FROM (b0_ecke LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) 
LEFT JOIN b3_tot ON (b3_ecke_w.Enr = b3_tot.Enr) AND (b3_ecke_w.Tnr = b3_tot.Tnr)
WHERE (((b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with the information on deadwood items (logs, stumps, etc.)

*SQL query for tbl_DE_Straten:*

``` 
SELECT b0_ecke.Tnr, b0_ecke.Enr, b3_ecke_w.Wa, b3_ecke_w.Begehbar, b3_ecke_w.Biotop, b3_ecke_w.NatWgF, b3_holzbod.WLT, b3_ecke_raum.Wb, b3_def_vegwlt.Wg, b3_bestock.natNaehe, b3_bestock.AntNatBa, b3_bestock.BestockTypFein, b3_bestock.BestockTypLN, b3_bestock.AlKl5 INTO tbl_DE_Straten
FROM ((((b0_ecke LEFT JOIN b3_bestock ON (b0_ecke.Tnr = b3_bestock.Tnr) AND (b0_ecke.Enr = b3_bestock.Enr)) LEFT JOIN b3_ecke_w ON (b0_ecke.Tnr = b3_ecke_w.Tnr) AND (b0_ecke.Enr = b3_ecke_w.Enr)) LEFT JOIN b3_holzbod ON (b0_ecke.Enr = b3_holzbod.Enr) AND (b0_ecke.Tnr = b3_holzbod.Tnr)) LEFT JOIN b3_ecke_raum ON (b0_ecke.Enr = b3_ecke_raum.Enr) AND (b0_ecke.Tnr = b3_ecke_raum.Tnr)) LEFT JOIN b3_def_vegwlt ON b3_ecke_raum.Wb = b3_def_vegwlt.Wb
WHERE (((b3_ecke_w.Wa)=1 Or (b3_ecke_w.Wa)=2 Or (b3_ecke_w.Wa)=3 Or (b3_ecke_w.Wa)=5 Or (b3_ecke_w.Wa)=7) AND ((b3_ecke_w.Begehbar)=1));
```

Creates a table with the information on tree compositions.

## Additional data

The following data have been prepared from publicly available information or published data by the author and are provided with this repository. Make sure that the following files are stored within the folder "00_Data/Git_files".

| Name                  | Description           | Source                                | 
|-----------------------|-----------------------|---------------------------------------|
| NFI_abiotic_data.csv  | Abiotic data for National Forest Inventory points | Created in QGIS and R from publicly available information | 
| edible_fungi_list.txt | List of edible fungi species | published as Supplementary Table 9 in Felipe-Lucia, M.R. et al. NatCommun 9, 4839 (2018) doi:10.1038/s41467-018-07082-4 |
| plant_edible.txt      | List of edible plant species | published as Supplementary Table 10 in Felipe-Lucia, M.R. et al. NatCommun 9, 4839 (2018) doi:10.1038/s41467-018-07082-4 |
| cultural_plants.txt   | List of culturally interesting plant species | published as Supplementary Table 11 in Felipe-Lucia, M.R. et al. NatCommun 9, 4839 (2018) doi:10.1038/s41467-018-07082-4 |
| i2_SSA_2.csv          | Forest structural stand attributes | Calculated from Schall & Christian Ammer. Biodiversity Exploratories Information System (2017): https://www.bexis.uni-jena.de/PublicData/PublicData.aspx?DatasetId=18268 |
| i2_SUB_SSA_2.csv      | Forest structural stand attributes from simulated angle counts |Calculated from Schall & Christian Ammer. Biodiversity Exploratories Information System (2017): https://www.bexis.uni-jena.de/PublicData/PublicData.aspx?DatasetId=18268 |

If the precise geographic coordinates of the National Forest Inventory are available to the user, the following files can be used to assign the abiotic variables to the data. Download the data as instructed and save in the folder "00_Data" (or in the indicated folder). Further preparation steps are provided in "01.2_Geographic_Environment_Information.R". 

For the default case (coordinates are not available), the provided file "NFI_abiotic_data.csv" contains the abiotic information and the anonymized coordinates are used to reproduce the figures. Hence, the data listed in the table are not needed to run the scripts.

| Name                  | Description      | Instructions                                           |Source                         | 
|-----------------------|------------------|--------------------------------------------------------|-------------------------------|
| wrblv11.tif           | soil properties  | save in folder "00_Data/Soil_properties_EU"            | <https://esdac.jrc.ec.europa.eu/content/european-soil-database-v2-raster-library-1kmx1km#tabs-0-description=0> |
| Clay_eu23.tif         | Clay content (%) | save in folder "00_Data/Soil_properties_EU"            | <https://esdac.jrc.ec.europa.eu/content/topsoil-physical-properties-europe-based-lucas-topsoil-data> | 
| Silt_eu23.tif         | Silt content (%) | save in folder "00_Data/Soil_properties_EU"            | <https://esdac.jrc.ec.europa.eu/content/topsoil-physical-properties-europe-based-lucas-topsoil-data> | 
| Sand_eu23.tif         | Sand content (%) | save in folder "00_Data/Soil_properties_EU"            | <https://esdac.jrc.ec.europa.eu/content/topsoil-physical-properties-europe-based-lucas-topsoil-data> |
| PhysGru1000_250.tif   | Soil depth       | close pop-up window and click "georeferenziertes TIFF" | <https://produktcenter.bgr.de/terraCatalog/DetailResult.do?fileIdentifier=faee466f-27c1-44c9-bcb8-3c2bec8b1116> |
| dgm200_tum32s.tif     | Elevation        | save in the folder "00_Data/Git_files/DEM_Deutschland" | provided with this repository. Calculated in QGIS from digital elevation model (dgm200): <https://gdz.bkg.bund.de/index.php/default/catalog/product/view/id/756/s/digitales-gelandemodell-gitterweite-200-m-dgm200/category/8/?___store=default>  |
| slope_percent_DE.tif  | Slope            | save in the folder "00_Data/Git_files/DEM_Deutschland" | provided with this repository. Calculated in QGIS from dgm200 |
| aspect_azimuth_DE.tif | Aspect           | save in the folder "00_Data/Git_files/DEM_Deutschland" | provided with this repository. Calculated in QGIS from dgm200 | 


Additional data from the Biodiversity Exploratories will be made publicly available once the embargo time has passed. The following files can then be downloaded from <https://www.bexis.uni-jena.de/PublicData/About.aspx>. Until then, data can be requested directly from the data owners. To ensure reproducability of the published analysis, dummy versions of those datasets are also created within the R-script "01.3_Check_prepare_data.R". Those dummy data contain randomly assigned values for each plot taken from the range of the observed values.

|DatasetId    |  Description                                            | Data owner information    |
|-------------|---------------------------------------------------------|---------------------------|
| 15386       | Dead wood inventory                                     | Tiemo Kahl, Jürgen Bauhus (University Freiburg)|
| 14686       | Texture of the mineral soil from soil sampling in 2011  | Ingo Schöning, Emily Solly, Theresa Klötzing, Susan Trumbore, Marion Schrumpf (BGC Jena)|
| 16666       | Root decomposition                                      | Emily Solly, Ingo Schönig, Theresa KlÃ¶tzig, Marion Schrumpf (BGC Jena)|
| 21449/15190 | Bird survey                                             | Swen Renner, Kirsten Jung, Marco Tschapka (University Ulm)|
| NA          | Forest soil depth                                       | Ingo Schöning (BGC Jena) |
| NA          | Volume of living and dead trees (harvV_dwV_stockPK_2012)| Tiemo Kahl, Jügen Bauhus (University Freiburg)|

In addition to raw datasets, a number of support files are used during the analysis. Those are provided with this repository and should be saved in the folder "00_Data/Git_files":

* lookup_PlotIDs
* lookup_table_predictors
* predictive_equations.RData

## How to use the R-Scripts

The R-scripts should be saved in the folder 01_Scripts. All file names begin with a number indicating the order in which the scripts need to be run. Scripts beginning  with "00_" need to be run first as they ensure that all libraries and functions are available. Depending on your personal setup (e.g. if you use Rprojects in Rstudio) not all scripts beginning with "00_" need to be run every time.

### Reproduce the analyses and figures

In order to reproduce the full published analyses and all figures, you need to run all scripts from "01.1_Data_import.R" to "05.4_FigS06_CrossValidation". If you want to only reproduce a specific figure, it is recommended to run all scripts up to the respective script, but at least up to "03.2_Predict_ES_proxies.R".

### Apply the predictive equations/production functions to your own data

The fitted models containing the parameters of the predictive equations are provided as a RData-file with this repository. If you want to use those models to predict ecosystem service proxies for your data, you will need to run only the scripts beginning with "00_" and the script "06.1_Predictive_equations.R".


Last updated: 11.04.2020