##-------------------------------------------------------------------------------------------------------------------
## Calculation and analysis of production functions between forest structural parameters and 
## ecosystem service proxies in Germany
#
## The analysis was conducted as part of the project "BioHolz-Projekt" (www.bioholz-projekt.de)
##    funded by the in the funding program 'Research for the Implementation of the National Biodiversity 
##    Strategy (F&U NBS)' by the German Federal Ministry for Education and Research (BMBF) and the 
##    German Federal Agency for Nature Conservation (BfN) with funds provided by the German Federal 
##    Ministry for the Environment, Nature Conservation, Building and Nuclear Safety (BMUB).
## The majority of data used were sampled within the Biodiversity Exploratories (www.biodiversity-exploratories.de)
#
## Analysis and results are published as:
## Simons Nadja K., María R. Felipe-Lucia, Peter Schall, Christian Ammer, Jürgen Bauhus, Nico Blüthgen,
## Steffen Boch, François Buscot, Markus Fischer, Kezia Goldmann, Martin M. Gossner, Falk Hänsel, 
## Kirsten Jung, Peter Manning, Thomas Nauss, Yvonne Oelmann, Rodica Pena, Andrea Polle, Swen C. Renner,
## Michael Schloter, Ingo Schöning, Ernst-Detlef Schulze, Emily F. Solly, Elisabeth Sorkau, 
## Barbara Stempfhuber, Tesfaye Wubet, Jörg Müller, Sebastian Seibold & Wolfgang W. Weisser (ADD YEAR):
## Forest inventories capture the multifunctionality of managed forests. ADD JOURNAL & DOI
# 
## R-scripts were mainly written by Nadja K. Simons (nadja.simons@tu-darmstadt.de)
## Last updated: 11. April 202
#
##------------------------------------------------------------------------------------------------------------------

# The predicted values of all forests are saved in NFI_Strata_ES_avg
summary(NFI_Strata_ES_avg)

# Select only those ecosystem service indicators which can be reliably extrapolated and
# get the set of forest inventory plots which contains values for all service indicators
NFI_Strata_ES_avg %>% 
  dplyr::select(one_of(c("Strata",ES_high_Rsquared$services))) %>% unique() %>% filter(!is.na(Root_decomp)) -> predicted_Strata_PCAdata # 53 Strata

row.names(predicted_Strata_PCAdata) <- predicted_Strata_PCAdata$Strata

# z-transform the variables prior to PCA (otherwise Euclidean distances cannot be used)
predicted_Strata_z<-decostand(predicted_Strata_PCAdata[,-1],method="standardize")

# Calculate the PCA
predicted_Strata.pca <- prcomp(predicted_Strata_z) # calculate PCA
summary(predicted_Strata.pca)            # get proportion of variance (axes 1-3 explain 88.1%)
plot(predicted_Strata.pca)

predicted_Strata.pca$rotation             # loadings of ecosystem service indicators
predicted_Strata.pca$x  -> loading_strata # loadings of plots

as.data.frame(loading_strata) %>% filter(PC1>0) %>% dim()

plot(predicted_Strata.pca$rotation)

# Prepare the PCA scores and loadings of the variables for plotting
predicted_Strata_PCA1_3<-as.data.frame(predicted_Strata.pca$x[,1:3]) #copy trait loadings for plotting

predicted_Strata_PCA1_3$PC1<-predicted_Strata_PCA1_3$PC1/max(abs(predicted_Strata_PCA1_3$PC1)) # scale trait loadings btw. 0 and 1
predicted_Strata_PCA1_3$PC2<-predicted_Strata_PCA1_3$PC2/max(abs(predicted_Strata_PCA1_3$PC2))
predicted_Strata_PCA1_3$PC3<-predicted_Strata_PCA1_3$PC3/max(abs(predicted_Strata_PCA1_3$PC3))
predicted_Strata_PCA1_3$PC1<-predicted_Strata_PCA1_3$PC1*0.6                # scale trait loadings to axis limits of species loadings
predicted_Strata_PCA1_3$PC2<-predicted_Strata_PCA1_3$PC2*0.6
predicted_Strata_PCA1_3$PC3<-predicted_Strata_PCA1_3$PC3*0.6

## Add the strata information to the PCA results

predicted_Strata_PCA1_3$Strata_1 <- predicted_Strata_PCAdata$Strata

# We need to calculate average values per strata
NFI_StrataSelection_1BT %>% dplyr::select(Strata_1,Wg,AntNatBa,BestockTypFein,AlKl5) %>%
  group_by(Strata_1) %>% summarise_all(list(mean))-> NFI_Strata_1BT_Info

NFI_Strata_1BT_Info %>% inner_join(predicted_Strata_PCA1_3) -> NFI_StrataSelection_1BT_PCA
NFI_StrataSelection_1BT_PCA %>% filter(!is.na(PC1)) -> NFI_StrataSelection_1BT_PCA


###----------------------------------
### Combined figure
###----------------------------------
ES_plotting_labels_PCA <- c("Root decomp.","Dung decomp.","Nitrification","Phosphorus","Mycorrhiza","Saprotrophs",
                            "Carbon in trees","Bark beetle control","Soil carbon","Temperature regulation",
                            "Productivity","Edible plants","Flowers") 

ES_plotting_x_coord_PCA <- predicted_Strata.pca$rotation[1:13]+
  c(0,0.15,0.1,0.13,-0.12,0,
    0.05,0.1,-0.12,-0.2,
   -0.08,-0.05,0.1)
ES_plotting_y_coord_PCA <- predicted_Strata.pca$rotation[14:26]+
  c(-0.025,0,0.01,0,0,-0.025,
    0.01,-0.03,0,0.02,
    -0.025,-0.02,0)

# Make the graph
tiff("../02_Output/Fig_01_predicted_Strata_PCA.tiff", width = 30, height = 15, units = 'cm', res = 300, compression = 'lzw')

layout(matrix(c(1,2,3,4,1,5,3,6),ncol=4, byrow = TRUE),widths = c(10,5,10,5))

# plot with tree composition
par(mar=c(5,5,3,0))
plot(x=NFI_StrataSelection_1BT_PCA$PC1,y=NFI_StrataSelection_1BT_PCA$PC2,
     pch=16, cex=1,col=as.character(BestockTyp_clr$color_BT[as.factor(NFI_StrataSelection_1BT_PCA$BestockTypFein)]),
     xlim=c(-0.6,0.6),ylim=c(-0.6,0.6),las=1,cex.lab = 1.5,cex.axis = 1.2,
     xlab = "PC1 (46.57%)",ylab="PC2 (25.19%)")
abline(0,0,lty=2,col="grey")
abline(v=0,lty=2,col="grey")
arrows(x0=0,y0=0,x1=predicted_Strata.pca$rotation[1:13],y1=predicted_Strata.pca$rotation[14:26], 
       col="black",length = 0.125)
text(x = ES_plotting_x_coord_PCA,y=ES_plotting_y_coord_PCA,labels = ES_plotting_labels_PCA,col="black")
text(x = -0.55, y = 0.6, labels = "a", font = 2, cex = 2)

# map with tree species composition
par(mar=c(1,1,1,1))
plot(germany_tmerc) 
# map.scale(relwidth = 0.5,ratio = FALSE)
points(NFI_plots_ESavg.spdf, 
       col=as.character(NFI_plots_ESavg.spdf@data$color_BT), pch=20, cex = 0.1)

# plot with geographic area
par(mar=c(5,5,3,0))
plot(x=NFI_StrataSelection_1BT_PCA$PC1,y=NFI_StrataSelection_1BT_PCA$PC2,
     pch=16, cex=1,col=as.character(Wg_clr$color_Wg[as.factor(NFI_StrataSelection_1BT_PCA$Wg)]),
     xlim=c(-0.6,0.6),ylim=c(-0.6,0.6),las=1,cex.lab = 1.5,cex.axis = 1.2,
     xlab = "PC1 (46.57%)",ylab="PC2 (25.19%)")
abline(0,0,lty=2,col="grey")
abline(v=0,lty=2,col="grey")
arrows(x0=0,y0=0,x1=predicted_Strata.pca$rotation[1:13],y1=predicted_Strata.pca$rotation[14:26], 
       col="black",length = 0.125)
text(x = ES_plotting_x_coord_PCA,y=ES_plotting_y_coord_PCA,labels = ES_plotting_labels_PCA,col="black")
text(x = -0.55, y = 0.6, labels = "b", font = 2, cex = 2)

# map with geographic area
par(mar=c(1,1,1,1))
plot(germany_tmerc) 
# map.scale(ratio = FALSE)
points(NFI_plots_ESavg.spdf, 
       col=as.character(NFI_plots_ESavg.spdf@data$color_Wg), pch=20, cex = 0.1)

# Legend for trees species composition
par(mar=c(2,1,2,1))
plot(1, type="n", axes=F, xlab="", ylab="",ylim=c(0,150),xlim=c(0,0))
legend(x="center", ncol=1,bty = "n", cex = 1.2,
       legend=c("Spruce","Spruce + D","Spruce + D + C","Pine","Pine + D","Beech","Beech + D","Beech + C",
                "Beech + D + C","Oak + D","Other deciduous"),
       title = "Tree species composition",
       fill = as.character(BestockTyp_clr$color_BT))

# Legend for geographic area
par(mar=c(2,0,2,0))
plot(1, type="n", axes=F, xlab="", ylab="",ylim=c(0,150),xlim=c(0,0))

legend(x="center", ncol=2,bty = "n",cex = 1.2,
       legend=levels(as.factor(NFI_StrataSelection_1BT_PCA$Wg)),
       title = "Geographic area",
       fill = as.character(Wg_clr$color_Wg))

dev.off()